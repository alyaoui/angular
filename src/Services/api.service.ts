import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiUrl = environment.apiUrl;
  private enableDebug = environment.enableDebug;
  constructor(private http: HttpClient) { }

  getUsers(eventId: number) {
    return this.http.get(this.apiUrl + 'event/'+eventId+'/users');
  }
  login(login:string,password:string) {
    return this.http.post(this.apiUrl + 'auth-tokens',{login,password});
  }
}
