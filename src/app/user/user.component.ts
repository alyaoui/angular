import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../Services/api.service';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {

  @ViewChild('userTable') userTable: any;
  public users: any[] = [];

  constructor(private apiService: ApiService) { }

  ngOnInit() {
     

    // Dynamically load jQuery using the script.js file
    const script = document.createElement('script');
    script.src = 'https://code.jquery.com/jquery-3.6.0.min.js';
    script.type = 'text/javascript';
    script.async = true;
    script.onload = () => this.initializeDataTable();
    document.head.appendChild(script);
  }
  private initializeDataTable(): void {
    this.apiService.getUsers(119).subscribe((response: any) => {
      this.users = response;
      $(this.userTable.nativeElement).DataTable({
        data: this.users,
        columns: [
          { title: 'lastName', data: 'lastName' },
          { title: 'firstName', data: 'firstName' }
        ]
      });
    });
  }
}
