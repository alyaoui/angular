import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../Services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  public login = '';
  public password ='';

  constructor(
    private router: Router,
    private apiService: ApiService
  ) {}

  signIn() {

    this.apiService.login(this.login, this.password ).subscribe(
      (response: any) => {
        localStorage.setItem('token', response.token);
        this.router.navigate(['/users']);
      },
      error => {
        console.error(error);
      }
    );
  }

  isLoggedIn(): boolean {
    const token = localStorage.getItem('token');
    return token ? true : false;
  }
}
